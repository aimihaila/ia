
import os
import random
import xml.etree.ElementTree
import datetime
from threading import Timer

out_of_time=False
knowledge_database_dir=r"D:\facultate\ia\repo\Knowledge Database"

def time_ran_out():
    print('\nYou didn\'t answer in time! \nAre you still there?')
    global out_of_time
    out_of_time=True

non_active_user_quotes = [
    'bye',
    'no',
    'done',
    'goodbye',
    'later'
]

bot_pasive_questions = [
    'Would you like to ask me a question?',
    'That was a good question. What about another one?',
    'Next question, please!',
    'I allow you to ask me another question!'
]

bot_active_questions = [
    "In what year of faculty are you?",
    "What was your favourite subject so far?",
    "Who is your "
    "Where do you go after classes?",
    "What do you like to do in weekends?",
    "What is your best friends name?",
]


def welcome_user():

    user_name = input("Hello human! What is your name?\n").lower().strip()
    user_surname = input("What a lovely name! Could you also tell me your surname?\n").lower().strip()
    user_birth_date = input("What is the date of your birth?\n").lower().strip()
    type_of_conv = input("Between you and me, who would you like to ask the questions?\n").lower().strip()
    while type_of_conv not in ["you", "me"]:
        type_of_conv = input("Please tell me who would you like to ask the questions? You or me?\n").lower().strip()
    print("Thank you! Now I'll remember you every time you'll want to chat with me!")

    return user_name, user_surname, user_birth_date, type_of_conv


def handle_question(question, user_data):

    answer = user_data.get(question, None)

    if answer is not None:
        return answer
    else:
        user_data[question] = input("Sorry! I don't know the answer for this question yet! "
                                    "What is your answer for this question?\n").lower().strip()
        return "Thank you very much for your answer!"

def handle_answer(question, answer, user_data):
    check_answer = user_data.get(question, None)

    if check_answer is not None and check_answer==answer:
        new_answer = input("You already gave me this answer for another question.\nPlease give me other answer.")
        user_data[question] = new_answer
    else:
        user_data[question] = answer
        return "Thank you very much for your answer!"


def get_user_data(user_name, user_surname, user_birth_date):
    user_knowledge_file = user_name + '_' + user_surname + '_' + user_birth_date + ".txt"
    all_users_knowledge_files = os.listdir(knowledge_database_dir)
    user_data = {}

    if user_knowledge_file in all_users_knowledge_files:

        user_knowledge_file_content = open(os.path.join(knowledge_database_dir,user_knowledge_file), 'r')
        for line in user_knowledge_file_content:
            question = line.split('|')[0].lower().strip()
            answer = line.split('|')[1].lower().strip()
            user_data[question] = answer

    else:
        print("OH! A new user! That makes me extremely happy as I can learn even more.")

    return user_data

'''
def get_my_data():
    my_data = {}
    aim_bot_knowledge_file = open(os.path.join(knowledge_database_dir,"bot.txt"), 'r')
    for line in aim_bot_knowledge_file:
        question = line.split('|')[0].lower()
        answer = line.split('|')[1].lower()
        my_data[question] = answer

    return my_data
'''


def save_data(user_name, user_surname, user_birth_date, user_data):
    user_knowledge_file = user_name + '_' + user_surname + '_' + user_birth_date + ".txt"
    user_knowledge_file_content = open(os.path.join(knowledge_database_dir,user_knowledge_file), 'w')

    for question in user_data:
        user_knowledge_file_content.write(question + '|' + user_data[question] + '\n')

def save_aiml_data(user_name, user_surname, user_birth_date, user_data):
    aiml_file = user_name + '_' + user_surname + '_' + user_birth_date + ".aiml"
    aiml_file_content = open(os.path.join(knowledge_database_dir, aiml_file), 'w')

    aiml_file_content.write('<aiml>\n')

    for question in user_data:
        aiml_file_content.write('\t<category>\n')
        aiml_file_content.write('\t\t<pattern>' + question.upper() + '</pattern>\n')
        aiml_file_content.write('\t\t<template>\n')
        aiml_file_content.write('\t\t\t' + user_data[question] + '\n')
        aiml_file_content.write('\t\t</template>\n')
        aiml_file_content.write('\t</category>\n')

    aiml_file_content.write('</aiml>\n')
    aiml_file_content.close()

def talk_to_the_user():
    global out_of_time
    user_name, user_surname, user_birth_date, type_of_conv = welcome_user()
    user_active = True

    user_data = get_user_data(user_name, user_surname, user_birth_date)
    print("Hello, " + user_name + '!')
    if type_of_conv == "me":
        print("You wanted to ask me some questions!")
        while user_active:
            t = Timer(10, time_ran_out)
            t.start()
            question = input(random.choice(bot_questions) + '\n').lower().strip()
            if question is None or out_of_time:
                out_of_time = False
                t.cancel()
            elif question.lower().strip() in non_active_user_quotes:
                out_of_time = False
                t.cancel()
                user_active = False
            else:
                out_of_time = False
                t.cancel()
                print(handle_question(question, user_data))
    elif type_of_conv == "you":
        print("You wanted me to ask you some questions!")
        while user_active:
            t = Timer(10, time_ran_out)
            t.start()
            question = random.choice(bot_active_questions)
            answer = input(question + '\n').lower().strip()
            if answer is None or out_of_time:
                out_of_time = False
                t.cancel()
            elif answer.lower().strip() in non_active_user_quotes:
                out_of_time = False
                t.cancel()
                user_active = False
            else:
                out_of_time = False
                t.cancel()
                print(handle_answer(question, answer, user_data))

    print("Glad to talk to ya! Have a nice day!")
    save_data(user_name, user_surname, user_birth_date, user_data)
    save_aiml_data(user_name, user_surname, user_birth_date, user_data)
    # implement a list of goodbye quotes

talk_to_the_user()


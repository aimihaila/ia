import os
import random
import xml.etree.ElementTree
import nltk
import string
from mongo_service import MongoService
from threading import Timer

out_of_time=False
our_database = r'C:\FII\III_S1\IA\Lab1\Tema1\Knowledge Database'
stopwords, tokenizer = None, None
conn = MongoService()
user_basic_data = {
    'first_name': ["> Hello human! What is your name?\n> "],
    'surname': ["> What a lovely name! Could you also tell me your surname?\n> "],
    'birth_date': ["> What is the date of your birth?\nFormat is: dd.mm.yyyy\n> "]
}
user_data = {}
user_active = False

def time_ran_out():
    print('\nYou didn\'t answer in time! \nAre you still there?')
    global out_of_time
    out_of_time=True

def init_grammar():
    global stopwords, tokenizer
    # Get default English stopwords and extend with punctuation
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.extend(string.punctuation)
    stopwords.append('')

    # Create tokenizer and stemmer
    tokenizer = nltk.tokenize.WordPunctTokenizer()


def match_sentence(a, b, matching_percentage=0.7):
    """Check if a and b are matches."""
    # tokens_a = [token.lower().strip(string.punctuation)
    #             for token in tokenizer.tokenize(a) if token.lower().strip(string.punctuation) not in stopwords]
    # tokens_b = [token.lower().strip(string.punctuation)
    #             for token in tokenizer.tokenize(b) if token.lower().strip(string.punctuation) not in stopwords]
    tokens_a = [token.lower().strip(string.punctuation) for token in tokenizer.tokenize(a)]
    tokens_b = [token.lower().strip(string.punctuation) for token in tokenizer.tokenize(b)]

    # Calculate Jaccard similarity
    try:
        ratio = len(set(tokens_a).intersection(tokens_b)) / float(len(set(tokens_a).union(tokens_b)))
    except:
        ratio = 0
    return ratio >= matching_percentage


non_active_user_quotes = [
    'bye',
    'no',
    'done',
    'goodbye',
    'later'
]

questions = [
    "In what year of faculty are you?",
    "What was your favourite subject so far?",
    "Who is your ",
    "Where do you go after classes?",
    "What do you like to do in weekends?",
    "What is your best friends name?",
]


def user_initialization():
    global user_data, questions

    for key in user_basic_data:
        user_basic_data[key].append(input(user_basic_data[key][0]).lower().strip())
    if conn.user_exists(user_basic_data['first_name'][1], user_basic_data['surname'][1], user_basic_data['birth_date'][1]):
        user_data = conn.get_user_data(
            user_basic_data['first_name'][1],
            user_basic_data['surname'][1],
            user_basic_data['birth_date'][1]
        )
    else:
        user_data = conn.insert_user(
            user_basic_data['first_name'][1],
            user_basic_data['surname'][1],
            user_basic_data['birth_date'][1]
        )
    choice = input("> Thank you! Would you like to ask me questions or do you prefer to let me know you, {}?\n"
                   "> Please answer with 'you' for me to ask or 'me' for you to ask\n> ".
                   format(user_basic_data['first_name'][1]))
    return choice

def find_answer(answer):
    for k,v in user_data:
        if v == answer and v.lower() not in ["yes", "no", "dk", "don't know"]:
            return True
    return False

def user_asks():
    global user_active, out_of_time
    print('> I am waiting for your questions')
    robo_data = conn.get_bot_knowledge()
    while user_active:
        t = Timer(30, time_ran_out)
        t.start()
        my_answer = ''
        question = input('').lower().strip()
        for q in robo_data:
            if q not in ['_id', 'name'] and q == question:
                my_answer = robo_data[q]
                break
            if q not in ['_id', 'name'] and match_sentence(question, q):
                my_answer = robo_data[q]
                break
        if answer is None or out_of_time:
            out_of_time = False
            t.cancel()
        elif not my_answer:
            out_of_time = False
            t.cancel()
            print('> I don\'t know what to respond. Please help me and tell me the answer')
            your_answer = input('').lower().strip()
            robo_data[question] = your_answer
            conn.update_bot_knowledge(question, your_answer)
            print('I learned a new thing')
        else:
            out_of_time = False
            t.cancel()
            print("> {}".format(my_answer))

    print("Glad to talk to ya! Have a nice day!")


def bot_asks():
    global user_active, user_data, questions, out_of_time
    for q in user_data:
        if q not in ['first_name', 'surname', 'birth_date']:
            if q in questions:
                questions.remove(q)
    while user_active:
        t = Timer(30, time_ran_out)
        t.start()
        if not questions:
            print('I have learned a lot about you today. Thanks, I have nothing else to ask you.')
            break
        current_question = random.choice(questions) + '\n'
        response = input(current_question).lower().strip()
        if answer is None or out_of_time:
            out_of_time = False
            t.cancel()
        elif response.lower().strip() in non_active_user_quotes:
            out_of_time = False
            t.cancel()
            user_active = False
        else:
            out_of_time = False
            t.cancel()

            if find_answer(response):
                new_response = input("You already gave me this answer for another question.\n"+
                                     "Please give me other answer.").lower().strip()
                user_data[current_question.lower().strip()] = new_response

            else:
                user_data[current_question.lower().strip()] = response
            conn.insert_question_and_answer(user_data['_id'], current_question.lower().strip(), response)
        questions.remove(current_question.strip())

    print("Glad to talk to ya! Have a nice day!")


def start():
    global user_active
    init_grammar()
    choice = user_initialization()
    user_active = True
    print("Hello, {}! Let's start our conversation.".format(user_basic_data['surname'][1]))
    if choice == 'me':
        user_asks()
    else:
        bot_asks()


if __name__ == "__main__":
    start()

from pymongo import MongoClient


class MongoService:

    def __init__(self):
        self.conn = MongoClient()
        self.db = self.conn['robo']

    def __del__(self):
        if self.conn:
            self.conn.close()

    def user_exists(self, first_name, surname, date):
        response = self.db['users'].find_one({
            'first_name': first_name,
            'surname': surname,
            'birth_date': date
        })
        if not response:
            return False

        return True

    def insert_user(self, first_name, surname, date):
        data = {
            'first_name': first_name,
            'surname': surname,
            'birth_date': date
        }
        self.db['users'].insert_one(data)
        return data

    def get_user_data(self, first_name, surname, date):
        response = self.db['users'].find_one({
            'first_name': first_name,
            'surname': surname,
            'birth_date': date
        })
        if not response:
            return {}

        return response

    def insert_question_and_answer(self, _id, question, answer):
        self.db['users'].update_one({
            '_id': _id
        }, {
            '$set': {
                question: answer
            }
        })

    def get_bot_knowledge(self):
        response = self.db['bot'].find_one({
            'name': 'robo'
        })
        if not response:
            return {}

        return response

    def update_bot_knowledge(self, question, answer):
        self.db['bot'].update_one({
            'name': 'robo'
        }, {
            '$set': {
                question: answer
            }
        })

    def insert_robo(self):
        data = {
            'name': 'robo'
        }
        self.db['bot'].insert_one(data)
